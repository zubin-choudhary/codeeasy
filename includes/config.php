<?php


ob_start();
session_start();
//define include checker
define('included', 1);

//Including DB file
include('db.php');

// make a connection to mysql here
$conn = @mysql_connect (DBHOST, DBUSER, DBPASS);
$conn = @mysql_select_db (DBNAME);
if(!$conn){
	die( "Sorry! There seems to be a problem connecting to our database.");
}
$q = mysql_query("SELECT * FROM siteinfo");
$row = mysql_fetch_object($q);

// define site path
define('DIR',$row->dir);

// define admin site path
define('DIRADMIN',$row->diradmin);

// define site title for top of the browser
define('SITETITLE',$row->title);

// define site description for the homepage
define('SITEDESC',$row->sitedesc);


include('functions.php');
?>
